resource "kubernetes_secret" "backend_sa_key_secret" {
  metadata {
    name = "backend-sa-key-secret"
  }

  data = {
    "credentials.json" = base64decode(google_service_account_key.backend_sa_key.private_key)
  }

  depends_on = [
    google_container_cluster.primary_cluster,
  ]
}


resource "random_password" "flask_secret_key_secret" {
  length = 50
}

resource "kubernetes_secret" "flask_secret_key_secret" {
  metadata {
    name = "flask-secret-key-secret"
  }

  data = {
    flask_secret = random_password.flask_secret_key_secret.result
  }

  depends_on = [
    google_container_cluster.primary_cluster,
  ]
}


resource "random_password" "jwt_secret_key_secret" {
  length = 50
}

resource "kubernetes_secret" "jwt_secret_key_secret" {
  metadata {
    name = "jwt-secret-key-secret"
  }

  data = {
    jwt_secret_key = random_password.jwt_secret_key_secret.result
  }

  depends_on = [
    google_container_cluster.primary_cluster,
  ]
}

resource "kubernetes_secret" "redis_host" {
  metadata {
    name = "redis-host-secret"
  }

  data = {
    redis_host = google_redis_instance.cache.host
  }

  depends_on = [
    google_container_cluster.primary_cluster,
  ]
}

resource "kubernetes_secret" "backend_cloudsql_user_secret" {
  metadata {
    name = "backend-cloudsql-user-secret"
  }

  data = {
    username = google_sql_user.backend_cloudsql_user.name
    password = google_sql_user.backend_cloudsql_user.password
  }

  depends_on = [
    google_container_cluster.primary_cluster,
  ]
}