# Infra
resource "google_sql_database_instance" "postgres" {
  project = var.project
  region  = var.region
  # Upon deletion of this resource, the name field is still unavailable up to two months
  name             = var.cloudsql_instance_name
  database_version = var.cloudsql_database_version

  settings {
    tier      = var.cloudsql_database_tier
    disk_type = var.cloudsql_disk_type
    disk_size = var.cloudsql_disk_size
  }
}
# Turns out this is multizone. 3 zones. 3 instances => 9 addresses... This could be causing the issue.
resource "google_container_cluster" "primary_cluster" {
  project  = var.project
  location = var.region

  name               = var.gke_cluster_name
  initial_node_count = var.gke_cluster_node_count

  min_master_version = var.gke_min_master_version

  node_config {
    machine_type = var.gke_cluster_machine_type
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.full_control",
      "https://www.googleapis.com/auth/sqlservice.admin",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/compute",
    ]
  }

  ip_allocation_policy {
  }
}

resource "google_redis_instance" "cache" {
  project        = var.project
  region         = var.region
  name           = var.memorystore_name
  tier           = var.memorystore_tier
  memory_size_gb = var.memorystore_memory_size_gb
  redis_version  = var.memorystore_redis_version
}

resource "google_compute_global_address" "backend_dev_permanent_ip" {
  project      = var.project
  name         = var.backend_dev_permanent_ip_name
  address_type = "EXTERNAL"
}