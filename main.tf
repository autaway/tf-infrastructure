provider "google" {
  credentials = file(var.credentials_file)
  project     = var.project
  region      = var.region
  version     = "3.7.0"
}

terraform {
  backend "gcs" {
    credentials = "GCP_SERVICE_ACCOUNT_CREDENTIALS.json"
    bucket      = "jowler-tf-state-bucket"
    prefix      = "terraform/state"
  }
}

provider "kubernetes" {
  version = "1.6.2"
}

provider "random" {
  version = "2.2"
}