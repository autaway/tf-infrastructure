resource "google_storage_bucket" "customer_documents" {
  name     = var.gcs_customer_binaries_bucket_name
  location = var.region
}