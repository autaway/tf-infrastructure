# General
variable "region" {
  type = string
}

variable "project" {
  type = string
}

variable "credentials_file" {
  type = string
}


# variable "backend_state_bucket" {
#   type = string
# }

# variable "backend_state_bucket_prefix" {
#   type = string
# }

# GKE
variable "gke_cluster_name" {
  type = string
}

variable "gke_cluster_machine_type" {
  type = string
}

variable "gke_cluster_node_count" {
  type = string
}

variable "gke_min_master_version" {
  type = string
}


# CloudSQL
variable "cloudsql_instance_name" {
  type = string
}

variable "cloudsql_database_version" {
  type = string
}

variable "cloudsql_disk_type" {
  type = string
}

variable "cloudsql_disk_size" {
  type = string
}

variable "cloudsql_database_tier" {
  type = string
}

# Memorystore
variable "memorystore_name" {
  type = string
}

variable "memorystore_tier" {
  type = string
}

variable "memorystore_memory_size_gb" {
  type = number
}

variable "memorystore_redis_version" {
  type = string
}

# IAM
variable "service_accounts" {
  type = map
}

# GCS
variable "gcs_customer_binaries_bucket_name" {
  type = string
}

# Networking
variable "backend_dev_permanent_ip_name" {
  type = string
}

# Outputs
output "cloudsql_backend_user_password" {
  value       = random_password.backend_cloudsql_user_password.result
  description = "Postgres password for backend. Store for personal use!"
}

output "backend_dev_permanent_ip" {
  value       = google_compute_global_address.backend_dev_permanent_ip.address
  description = "Permanent public IP for the Jowler backend server. Use in conjunction with a DNS A record"
}