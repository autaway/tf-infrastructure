# Terraform Infra 
This is our version-controlled infrastructure managed by Terraform. The state of the infrastructure is maintained in a Cloud Storage bucket. It is discouraged to deploy any new infra without registering it here.

## Plans for the future 
1. Implement Atlantis CI/CD
2. Separate live variables from infrastructure declaration
3. Modularisation through terragrunt

N.B. Don't forget to set your kubernetes context to the right GKE cluster!

## Commands
1. `terraform init`  to download all required dependencies and set up remote state store  
2. `terraform plan -out plan` to dump plan for terraform to execute  
3. `terraform apply plan` to execute the synthesised plan  
4. `terraform validate` to perform static validation  
5. `terraform state pull > old_terraform.tfstate` to pull the remote state and store it locally as rollback backup  
6. `terraform show` to get the current state of the infra from remote storage  
7. `terraform destroy -target=resource_type.resource_name` to delete a specific resource  
8. `terraform state rm resource_type.resource_name` to stop tracking a potentially corrupted resource in the state file

## How to bootstrap a GCP project  
0. `gcloud` switch to the right project and check your kubectx (secret creation will fail without this). Delete `~/.kube` if you have to. Make sure the `.terraform` folder is deleted
1. Enable Cloud Resource Manager API
2. Enable Cloud SQL Admin API
3. Enable Kubernetes Engine API
4. Enable GKE and Memorystore APIs by clicking into them
5. Create a bucket called `autaway-tf-dev` for remote terraform state. Name depends on environment
6. Create `tf-provisioner` service account
7. Create a custom `Provisioner` role for `tf-provisioner` service account and give it 

## Android specific
1. Enable Google Play Android Developer API 
2. Go through this setup https://developers.google.com/android-publisher/api-ref/edits/bundles/upload
3. Link project on Google Play Developer Console

- `Kubernetes Engine Cluster Admin` 
- `Security Admin` 
- `Project IAM Admin` 
- `Storage Admin`
- `Cloud SQL Admin` 
- `Compute Admin` 
- `Cloud MemoryStore Redis Admin` 
- `Service Account Key Admin` 
- `Service Account Admin` 
- `Service Account User` 
- `Compute Instance Admin` 
- `Kubernetes Engine Developer (container.secrets.get)`  
privileges. That way terraform will not touch `tf-provisioner`'s roles  

## Note about deployment
Because service account resources are only known after applying them, we want to run  
`terraform apply -target google_service_account.service_account`  
and only then `terraform plan` and `terraform apply`. That way, we'll be able to get a plan for the role creation.  