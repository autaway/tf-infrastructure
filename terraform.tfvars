# General
project          = "jowler-dev"
region           = "europe-west2"
credentials_file = "GCP_SERVICE_ACCOUNT_CREDENTIALS.json"
# backend_state_bucket        = "autaway-dev-tf"
# backend_state_bucket_prefix = "terraform/state"

# GKE
gke_cluster_name         = "dev-cluster"
gke_cluster_machine_type = "n1-standard-1"
# TODO: fails for 3; succeeds for 1. Max address in use issue. Is it trying to create a surrogate node first?
gke_cluster_node_count = 1
gke_min_master_version = "1.15.8-gke.3"

# CloudSQL
cloudsql_instance_name    = "dev-db"
cloudsql_database_version = "POSTGRES_11"
cloudsql_database_tier    = "db-g1-small"
cloudsql_disk_type        = "PD_SSD"
cloudsql_disk_size        = "10"

# Memorystore
memorystore_name           = "dev-redis"
memorystore_tier           = "BASIC"
memorystore_memory_size_gb = 1
memorystore_redis_version  = "REDIS_4_0"

# Cloud Storage
gcs_customer_binaries_bucket_name = "customer-binary-files-bucket"


# IAM
# name | description
service_accounts = {
  backend-sa            = ["backend-sa", "Service account for backend application to access GCP services"],
  bitbucket-provisioner = ["bitbucket-provisioner", "An account for Bitbucket Pipelines to perform CI/CD on GCP"]
}

# Networking
backend_dev_permanent_ip_name = "backend-dev-permanent-ip"