# Service accounts
resource "google_service_account" "service_account" {
  for_each = var.service_accounts

  account_id   = each.key
  display_name = each.value[0]
  description  = each.value[1]
}

locals {
  service_account_roles = toset([
    "user:arthur@autaway.com roles/cloudsql.editor",
    "user:arthur@autaway.com roles/compute.viewer",
    "user:arthur@autaway.com roles/container.clusterViewer",
    "user:arthur@autaway.com roles/container.developer",

    "user:sujan.koju@autaway.com roles/cloudsql.editor",
    "user:sujan.koju@autaway.com roles/compute.viewer",
    "user:sujan.koju@autaway.com roles/container.clusterViewer",
    "user:sujan.koju@autaway.com roles/container.developer",
    "user:sujan.koju@autaway.com roles/viewer",

    "serviceAccount:${google_service_account.service_account["backend-sa"].email} roles/cloudsql.admin",
    "serviceAccount:${google_service_account.service_account["backend-sa"].email} roles/storage.admin",
    "serviceAccount:${google_service_account.service_account["backend-sa"].email} roles/redis.admin",
    "serviceAccount:${google_service_account.service_account["bitbucket-provisioner"].email} roles/compute.admin",
    "serviceAccount:${google_service_account.service_account["bitbucket-provisioner"].email} roles/container.admin",
    "serviceAccount:${google_service_account.service_account["bitbucket-provisioner"].email} roles/storage.admin",
  ])
}

resource "google_project_iam_member" "project_roles" {
  for_each = local.service_account_roles

  project    = var.project
  role       = element(split(" ", each.value), 1)
  member     = element(split(" ", each.value), 0)
  depends_on = [google_service_account.service_account]
}

# User/service account associations
locals {
  user_to_service_account_associations = toset([
    "${google_service_account.service_account["backend-sa"].name} user:arthur@autaway.com",
    "${google_service_account.service_account["bitbucket-provisioner"].name} user:arthur@autaway.com",
  ])
}

resource "google_service_account_iam_member" "service_account_users" {
  for_each = local.user_to_service_account_associations

  service_account_id = element(split(" ", each.value), 0)
  role               = "roles/iam.serviceAccountUser"

  member = element(split(" ", each.value), 1)
}

# Service account keys
resource "google_service_account_key" "backend_sa_key" {
  service_account_id = google_service_account.service_account["backend-sa"].name
}

# CloudSQL users
resource "random_password" "backend_cloudsql_user_password" {
  length = 50
}

resource "google_sql_user" "backend_cloudsql_user" {
  name     = "backend_cloudsql_user"
  project  = var.project
  instance = google_sql_database_instance.postgres.name
  password = random_password.backend_cloudsql_user_password.result
}